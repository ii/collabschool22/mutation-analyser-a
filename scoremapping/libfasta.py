"""pip install biopython"""
from Bio import SeqIO
class Entry:
    def __init__(self, identifier, sequence):
        self.identifier = identifier
        self.sequence = sequence

def get_entries(p):
    with open(p) as handle:
        for i,record in enumerate(SeqIO.parse(handle, "fasta")):
            yield (i,Entry(record.id,record.seq))
    
def read_fasta(p):
    with open(p) as handle:
        for i,record in enumerate(SeqIO.parse(handle, "fasta")):
            print(i,record.id,record.seq)
