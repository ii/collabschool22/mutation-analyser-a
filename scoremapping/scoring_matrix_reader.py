import pandas as pd

def scoring_matrix_reader(file):
    score_table = pd.read_table(file, sep = '\s+', index_col = 0)
    
    score_table['z_score'] = 0
    
    for i in range(len(score_table)):
        if score_table['P-index'][i] < 0 and score_table['I-index'][i] < 0:
            score_table.iat[i,2] = max(score_table['P-index'][i],score_table['I-index'][i])
        elif score_table['P-index'][i] > 0 and score_table['I-index'][i] > 0:
            score_table.iat[i,2] = min(score_table['P-index'][i],score_table['I-index'][i])
        else:
            score_table.iat[i,2] = round((score_table['P-index'][i]+score_table['I-index'][i])/2, 4)
    
    dict = score_table.drop(['P-index','I-index'], axis = 1).to_dict()
    
    return dict['z_score']
    