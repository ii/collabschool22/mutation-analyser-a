# score_octamer_list.copy
from scoremapping.octamerreader import octamer_read
def z_score_finder(sequence,score):
    mutation_analyser_input=[]
    octamer=octamer_read(sequence)
    for i in octamer:
        mutation_analyser_input.append(score[i.lower()])
    return mutation_analyser_input