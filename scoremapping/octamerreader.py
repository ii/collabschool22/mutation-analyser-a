#octamerreader.py
from Bio import SeqIO
def octamer_read(sequence1):
    l=list()
    for i in range(len(sequence1)-7):
        octamer=sequence1[i:i+8]
        #print(octamer)
        l.append(octamer)
    # print(l)
    return(l)
# from Bio import SeqIO
# record = SeqIO.read("octmerreaderdummy.txt","fasta")
# table = 11
# min_pro_len = 100
# for strand, nuc in [(+1, record.seq), (-1, record.seq.reverse_complement())]:
#     for frame in range(3):
#         length = 3 * ((len(record)-frame) // 3) #Multiple of three
#         for pro in nuc[frame:frame+length].translate(table).split("*"):
#             if len(pro) >= min_pro_len:
#                 print("%s...%s - length %i, strand %i, frame %i" \
#                     % (pro[:30], pro[-3:], len(pro), strand, frame))

# from Bio import SeqIO

# record = SeqIO.read("octmerreaderdummy.txt","fasta")
# table = 11
# min_pro_len = 100

# def find_orfs_with_trans(seq, trans_table, min_protein_length):
#     answer = []
#     seq_len = len(seq)
#     for strand, nuc in [(+1, seq), (-1, seq.reverse_complement())]:
#         for frame in range(8):
#             trans = nuc[frame:].translate(trans_table)
#             trans_len = len(trans)
#             aa_start = 0
#             aa_end = 0
#             while aa_start < trans_len:
#                 aa_end = trans.find("*", aa_start)
#                 if aa_end == -1:
#                     aa_end = trans_len
#                 if aa_end - aa_start >= min_protein_length:
#                     if strand == 1:
#                         start = frame + aa_start * 8
#                         end = min(seq_len, frame + aa_end * 8 + 8)
#                     else:
#                         start = seq_len - frame - aa_end * 8 - 8
#                         end = seq_len - frame - aa_start * 8
#                     answer.append((start, end, strand, trans[aa_start:aa_end]))
#                 aa_start = aa_end + 1
#     answer.sort()
#     return answer


# orf_list = find_orfs_with_trans(record.seq, table, min_pro_len)
# for start, end, strand, pro in orf_list:
#     print(
#         "%s...%s - length %i, strand %i, %i:%i"
#         % (pro[:30], pro[-3:], len(pro), strand, start, end)
#     )