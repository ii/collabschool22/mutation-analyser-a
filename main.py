import src.analyser_plot as ap

"""
Input
A FASTA file containing a reference sequence and optionally, mutated sequences. An example is test.fasta

Scoring
Obtain the enhancer/silencer score for each 8-mer along the sequence from octamers.txt. An 8-mer is an enhancer
sequence if both columns (P and I) are above 2.62. It is a silencer if both columns are below -2.62.

Visualisation
Show a graph of the score plotted over the text of the sequence, as shown below. Find a way of combining the 
separate P and I scores into a single meaningful plottable value.

Interactivity
The user should be able to hover the mouse cursor over any of the sequence text and type a different nucleotide
letter, with the effect on the graph immediately shown. Use the event handling features in matplotlib.
"""
from scoremapping.scoring_matrix_reader import scoring_matrix_reader as smr
from scoremapping.libfasta import get_entries, read_fasta


if __name__ == "__main__":
    fasta_file = 'data/test.fasta'
    score_file = 'data/score_file.txt'
    entries = dict(get_entries(fasta_file))
    sequence1 = entries[0].sequence
    score = smr(score_file)


    mutations = []
    for i in range(1,len(entries)):
        seq = entries[i].sequence
        mutations.append(seq)



    plot = ap.AnalyserPlot(sequence1,score,mutations)
    plot.start()