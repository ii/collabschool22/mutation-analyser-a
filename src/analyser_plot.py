# importing the necessary modules

from tkinter import X
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import time
import matplotlib
from matplotlib.widgets import TextBox
from scoremapping.z_score_reader import z_score_finder

matplotlib.use('qtagg')

class AnalyserPlot:
    # initialization
    def __init__(self,main_sequence,score_data,mutations):

        self.mutations = mutations
        self.mutation_count = 0
        self.zorder=6
        self.main_sequence= main_sequence
        self.score_data= score_data 
        self.plot_data =  z_score_finder(main_sequence, score_data)
        extra_chars = len(self.plot_data)
        self.labels =  [s for s in str(main_sequence[:extra_chars])]
        (self.figure, self.axes) = plt.subplots()
        self.figure.subplots_adjust(bottom=0.2)
        self.x = [str(i) for i in range(0,len( self.labels))]
        self.line, = self.axes.plot(self.x,self.plot_data,zorder = self.zorder)
        self.axes.set_xticks(range(0,len( self.labels)),self.labels,zorder=2)
        self.figure.canvas.mpl_connect('button_press_event', self.press)
        self.figure.canvas.mpl_connect('button_release_event', self.release)

        self.axbox = self.figure.add_axes([0.4, 0.05, 0.1, 0.075]) #positioning for the textbox
        self.text_box = TextBox(self.axbox, "Modify ", textalignment="center")
        self.text_box.on_submit(self.update)
        self.text_box.disconnect(self.axbox)
        self.annot = self.axes.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
        bbox=dict(boxstyle="round", fc="w"),
        arrowprops=dict(arrowstyle="->"))
        self.annot.set_visible(False)
        self.figure.canvas.mpl_connect("motion_notify_event", self.hover)
        self.legend_list = ["Default"]



    def plot_mutations(self):
        i = 1
        for mutation in self.mutations:
            plot_data = z_score_finder(mutation, self.score_data)
            extra_chars = len(plot_data)
            labels = [s for s in str(mutation[:extra_chars])]
            x = [str(i) for i in range(0, len(labels))]
            self.axes.plot(x, plot_data,zorder=self.zorder-i)
            self.legend_list.append("Mutation {0}".format(i))
            i +=1
            self.mutation_count += 1

    def replot_data(self,scores,mod_label,x_index):
        line_2, = self.axes.plot(self.x,scores,zorder=self.zorder-self.mutation_count)
        # ("Mod",self.selected_nuc,self.x_index,"->",mod_label)
        if not ("Mod: {0} {1}->{2}".format(self.x_index,self.selected_nuc,mod_label)) in self.legend_list:
            self.legend_list.append("Mod: {0} {1}->{2}".format(self.x_index,self.selected_nuc,mod_label))
            self.axes.legend(self.legend_list)
            self.vLine = self.axes.vlines(x=x_index, ymin=self.axes.get_ylim()[0], ymax=scores[x_index], colors='gray', ls=':', lw=2, label='_nolegend_')
            plt.draw()

    # start event to show the plot
    def start(self):
        plt.legend(loc="upper left")
        self.plot_mutations()
        plt.show()  # display the plot

    # press event will keep the starting time when
    # press mouse button
    def press(self, event):
        self.start_time = time.time()

    # release event will keep the track when you release
    # mouse button
    def release(self, event):
        self.end_time = time.time()
        self.call(event)

    #this method is called each time text box is changed to update nessesary parameters
    # TODO add algorithm to get numeric values from sequence
    def update(self,exp):
        if exp == self.selected_nuc or exp=="" or (not exp in ["A","T","G","C"]):
            return
        position = self.x_index
        new_character = exp

        temp = list(self.main_sequence)
        temp[position] = new_character
        sequence2 = "".join(temp)
        plot_data_2 = z_score_finder(sequence2, self.score_data)
        self.replot_data(plot_data_2,exp,self.x_index)
       

    # get cursor coordinates
    def call(self, event):
         if event.inaxes == self.axes: 
            cont, ind = self.line.contains(event)
            if cont:
                self.x_index= ind["ind"][0]
                self.selected_nuc=self.labels[ind["ind"][0]]
                # print(self.x_index,self.selected_nuc)
                self.text_box.set_val(self.selected_nuc)  # Trigger `update` with the initial string.
                
                

    def update_annot(self,ind,pos):
        text = (ind["ind"][0],":",self.labels[ind["ind"][0]])
        self.annot.set_text(text)
        self.annot.xy=(pos)
        self.annot.get_bbox_patch().set_alpha(1)


    def hover(self,event):     

        vis = self.annot.get_visible()
        if event.inaxes == self.axes: 
          
            cont, ind = self.line.contains(event)
            
            if cont:
                x_index= ind["ind"][0]
                pos = [x_index,event.ydata]
                self.update_annot(ind,pos)
                self.annot.set_visible(True)
                self.figure.canvas.draw_idle()
            else:
                if vis:
                    self.annot.set_visible(False)
                    self.figure.canvas.draw_idle()

        


